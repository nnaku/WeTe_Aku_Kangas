'use strict';
var notes;


function itemExist(item){
	for (var i = 0; i < notes.length; ++i) {
		if(notes[i].title.toLowerCase() == item.toLowerCase()){
			++notes[i].quantity;
			return true;
		};
	}
	return false;
}

function addItem() {
	var textbox = document.getElementById('item');
	var itemText = textbox.value;
	textbox.value = '';
	textbox.focus();
	if(!itemExist(itemText)){
		var newItem = {title: itemText, quantity: 1};
		notes.push(newItem);	
	}

	saveList();
	loadList();
	
}

function displayList() {
	var table = document.getElementById('list');
	table.innerHTML = '';
	for (var i = 0; i<notes.length; i++) {
		var node = undefined;
		var note = notes[i];
		var node = document.createElement('tr');
		var html = '<td>'+note.title+'</td><td>'+note.quantity+'</td><td><a href="#" onClick="deleteIndex('+i+')">delete</td>';
		node.innerHTML = html;
		table.appendChild(node);
	}
}

function deleteIndex(i) {
	notes.splice(i, 1);
	saveList();
	loadList();
}

function saveList() {
    localStorage.notes = JSON.stringify(notes);
}

function loadList() {
    console.log('loadList');
    if (localStorage.notes) {
        notes = JSON.parse(localStorage.notes);
        displayList();
    }else{
    	notes = new Array();
    	console.log('no notes at localStorage!!!');
    	
    }
}

var button = document.getElementById('add');
button.onclick = addItem;