<?php

include("diceclasses.inc.php");

$faces = $_GET["faces"];
$throws = $_GET["throws"];
$p = $_GET["p"];
$material = $_GET["material"];


$results = array();

// make dice

if ($material){
    $dice = new PhysicalDice($faces, $p, $material);
    echo "The dice is made of " .$material. "!  <br><br>";
}
else{
    $dice = new Dice($faces, $p);
}


for ($i = 1; $i<=$throws; $i++) {
    $res = $dice->cast();
    $results[] = array('id' => strval($i), 'res' => strval($res));
}
$freqs = array();
for ($i = 1; $i<=$faces; $i++) {
    $freqs[] = array ('eyes' => strval($i), 'frequency' => strval($dice->getFreq($i)));
}

foreach ($results as $value){
    $numval = $value['res'];
    $allres = $allres + $numval;
}
$output = "<script>console.log( " .$allres. " ".$value. ");</script>";
echo $output;

$avgnum = $dice->average($allres, $throws);


echo json_encode(array('material'=>$material, 'p'=>$p,  'faces'=>$faces,'results'=>$results,'frequencies'=>$freqs, 'average' => $avgnum));


?>